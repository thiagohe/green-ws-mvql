package br.com.mv.integra.framework.exception;

public class MVQLException {
	Exception e;
	
	public MVQLException(Exception e) {
		this.e = e;
	}
	
	public String getErrorMessage() {
		String resultado = "";
		
		for(StackTraceElement el : e.getStackTrace()) {
			resultado += el.toString() + " ";
		}
		
		return resultado;
	}
}
