package br.com.mv.integra.query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MVQLValue {
	public static final String TO_DATE = "TO_DATE";
	public static final String QUOTE = "'";
	public static final String LPAR = "(";
	public static final String RPAR = ")";
	public static final String DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS = "YYYY-MM-DD HH24:MI:SS";
	public static final String COMMA = ",";
	public static final String NULL = "null";
	public static final String SP = " ";
	
	public enum MVQLType {String, Integer, Date, Double, Null};
	
	public Object value;
	public MVQLType type;
	
	public MVQLValue(Object value) {
		this.value = value;
		
		setMVQLType();
	}
	
	public void setMVQLType() {
		if (this.value instanceof String) {
			String date = null, time = null;
			Scanner scanner = new Scanner(value.toString());
			try {
				date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
				time = scanner.next(Pattern.compile("\\d{2}:\\d{2}:\\d{2}"));
			} catch (java.util.InputMismatchException e) {
			}

			if (date != null && time != null) {
				this.type = MVQLType.Date;			
			}  else {
				this.type = MVQLType.String;
			}
		} else if (value instanceof Integer) {
			this.type = MVQLType.Integer;
		} else if (value instanceof Double) {
			this.type = MVQLType.Double;
		} else {
			this.type = MVQLType.Null;
		}
	}
	
	public Object toValue() throws ParseException {
		Object resultado = null;
		
		if(this.type.equals(MVQLType.Date)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String dateInString = (String) value;
			resultado = new java.sql.Timestamp(sdf.parse(dateInString).getTime());
		} else if(this.type.equals(MVQLType.String)) {
			resultado = new String(this.value.toString());
		} else if(this.type.equals(MVQLType.Integer)) {
			resultado = new Integer(this.value.toString());
		} else if(this.type.equals(MVQLType.Double)) {
			resultado = new Double(this.value.toString());
		} else {
			resultado = null;
		}
		return resultado;
	}
	
	public String generateSQL() {
		String resultado = null;
		
		if (this.value instanceof String) {
			String date = null, time = null;
			Scanner scanner = new Scanner(value.toString());
			try {
				date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
				time = scanner.next(Pattern.compile("\\d{2}:\\d{2}:\\d{2}"));
			} catch (java.util.InputMismatchException e) {
			}

			if (date != null && time != null) {
				date = value.toString();
				resultado = TO_DATE + LPAR + QUOTE + date + QUOTE
						+ COMMA + QUOTE + DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR + SP;
			} else if (((String) value).equalsIgnoreCase(NULL)) {
				resultado = value.toString() + SP;
			} else {
				resultado = QUOTE + value.toString() + QUOTE + SP;
			}
		} else if (value instanceof Integer) {
			resultado = new Integer(value.toString()) + SP;
		}
		
		return resultado;
	}
	
}
