package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class NenhumRegistroInseridoException extends Exception {
	public NenhumRegistroInseridoException() {
		super(FachadaFramework.getResourceBundle().getString("nenhum_registro_inserido"));
	}
}
