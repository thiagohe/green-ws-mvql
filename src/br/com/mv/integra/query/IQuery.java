package br.com.mv.integra.query;

import java.io.IOException;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface IQuery {
	public Query parse(HashMap<String, Object> map, String returnColumn) throws JsonParseException, JsonMappingException, IOException;
}
