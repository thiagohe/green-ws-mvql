package br.com.mv.integra.query;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import br.com.mv.integra.repositorio.Repositorio;

public class MVQLJdbcCall extends SimpleJdbcCall {
	MapSqlParameterSource params;

	
	public MVQLJdbcCall() {
		super(Repositorio.getJDBCTemplate());
		
		this.params = new MapSqlParameterSource();

	}

	public Map<String, Object> execute() {
		return super.execute(this.params);
	}
}
