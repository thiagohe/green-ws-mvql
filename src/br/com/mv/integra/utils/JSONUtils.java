package br.com.mv.integra.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mv.integra.framework.FachadaFramework;
import br.com.mv.integra.framework.exception.MVQLException;
import br.com.mv.integra.framework.exception.VersaoInvalidaException;
import br.com.mv.integra.repositorio.ObjectDTO;
import oracle.sql.BLOB;

public class JSONUtils {
	public static final String ENTITY = "ENTITY_NAME";
	public static final String LBRACE = "{";
	public static final String RBRACE = "}";
	public static final String LBRAC = "[";
	public static final String LIST = "list";
	public static final String RBRAC = "]";
	public static final String SP = " ";
	public static final String COLON = ":";
	private static final String COMMA = ",";
	private static final String QUOTE = "\"";
	
	public static HashMap<String, Object> jsonToMap(String t) throws JsonParseException, JsonMappingException, IOException, VersaoInvalidaException {
        HashMap<String, Object> resultado = new HashMap<String, Object>();

        resultado = FachadaFramework.getFramework().getObjectMapper().readValue(t, new TypeReference<Map<String, Object>>(){});
        
        return resultado;
    }
	
	public static String listObjectDTOToJson(String entityName, List<ObjectDTO> list) throws JsonProcessingException, VersaoInvalidaException {
		StringBuilder resultado = new StringBuilder("");

		resultado.append(QUOTE + entityName + QUOTE + SP + COLON + SP + LBRAC);
		
		Iterator iteratorDTO = list.iterator();
		
		while(iteratorDTO.hasNext()) {
			ObjectDTO objectDTO = (ObjectDTO) iteratorDTO.next();
			
			resultado.append(mapToJsonItem(entityName, objectDTO.getMap()));

			if(iteratorDTO.hasNext()) resultado.append(COMMA + SP);			
		}
		
		resultado .append(RBRAC);		
		
		return resultado.toString();
	}
	
	public static String mapToJsonItem(String entityName, HashMap<String, Object> map) throws JsonProcessingException, VersaoInvalidaException {
		JSONUtils.removeUnsupportedTypes(map);
		
		String resultado = FachadaFramework.getFramework().getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(map.get(ENTITY));
		
		return resultado;		
	}
	
	private static void removeUnsupportedTypes(HashMap<String, Object> map) {
		Iterator iterator = map.values().iterator();
		
		while(iterator.hasNext()) {
			Object object = iterator.next();
			
			if(object instanceof HashMap) {
				JSONUtils.removeUnsupportedTypes((HashMap<String, Object>) object);
			}
			
			if(object instanceof BLOB) {
				iterator.remove();
			}
		}		
	}

	public static String toJson(String entityName, Object object) throws JsonProcessingException, VersaoInvalidaException {
		StringBuilder resultado = new StringBuilder("");
		
		resultado.append(LBRACE + SP);
		
		if(object instanceof HashMap) {
			resultado.append(JSONUtils.mapToJson(entityName, ((ObjectDTO) object).getMap()));
		} else if(object instanceof List) {
			resultado.append(JSONUtils.listObjectDTOToJson(entityName, (List<ObjectDTO>) object));
		}
		
		resultado.append(RBRACE);
		
		return resultado.toString();
	}
	
	public static String mapToJson(String entityName, HashMap<String, Object> map) throws JsonProcessingException, VersaoInvalidaException {
		String resultado = FachadaFramework.getFramework().getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(map);
		
		resultado = resultado.replace(ENTITY, entityName);
		
		return resultado.toString();		
	}	
	
	public static String exceptionToJson(MVQLException e) {
		String resultado = LBRACE + SP;
		
		resultado += "\"MVQL:ERROR\""+ SP;
		
		resultado += COMMA;
		
		resultado += "\""+e.getErrorMessage()+"\"";
		
		resultado += RBRACE;
		
		return resultado;
	}
}
