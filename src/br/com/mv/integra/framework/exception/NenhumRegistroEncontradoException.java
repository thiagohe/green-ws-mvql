package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class NenhumRegistroEncontradoException extends Exception {
	public NenhumRegistroEncontradoException() {
		super(FachadaFramework.getResourceBundle().getString("nenhum_registro_encontrado"));
	}
}
