package br.com.mv.integra.compiler.syntaxtree;

import br.com.mv.integra.compiler.parser.JSONParserVisitor;

public class JsonSQLAST implements JSONParserVisitor {
	SimpleNode root;
		
	public JsonSQLAST(SimpleNode root) {
		this.root = root;
	}
	
	public void start() {
		this.root.childrenAccept(this, null);
	}

	public Object visit(SimpleNode node, Object data) {
		node.childrenAccept(this, data);
		return data;
	}
	
	public String toString() {
		return "TBI";
	}

}
