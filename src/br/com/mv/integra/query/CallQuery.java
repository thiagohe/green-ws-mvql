package br.com.mv.integra.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class CallQuery extends Query {
	public enum CallQueryType {Complete, Simple};
	
	public String schema;
	public String packaging;
	public String callable;
	public String callableType;
	public CallQueryType type;
	public List<MVQLParameterType> parameterClauses;
	public List<MVQLValue> valueClauses;

	public static CallQuery parse(HashMap<String, Object> map)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		CallQuery resultado = null;
		String entity = null;

		resultado = new CallQuery();

		for (String key : map.keySet()) {		
			entity = key;
			
			String[] params = key.split("[.]");
			if(params.length > 2) {
				resultado.schema = params[0];
				resultado.packaging = params[1];
				resultado.callable = params[2];
				resultado.type = CallQueryType.Complete;
				
			} else {
				resultado.schema = params[0];
				resultado.callable = params[1];
				resultado.type = CallQueryType.Simple;
			}
			
			if(resultado.callable.contains(Query.FUNCTION)) {
				resultado.callableType = Query.FUNCTION;
			} else if(resultado.callable.contains(Query.PROCEDURE)) {
				resultado.callableType = Query.PROCEDURE;
			} else {
				resultado.callableType = Query.PROCEDURE;
			}
		}

		HashMap<String, Object> child = (HashMap<String, Object>) map.get(entity);

		resultado.parameterClauses = new ArrayList<MVQLParameterType>();
		resultado.valueClauses = new ArrayList<MVQLValue>();

		for (String parameterClause : child.keySet()) {
			MVQLParameterType parameter = new MVQLParameterType(parameterClause);
			MVQLValue childValue = new MVQLValue(child.get(parameterClause));

			resultado.parameterClauses.add(parameter);
			resultado.valueClauses.add(childValue);

		}

		return resultado;
	}

	public String generateSQL() {
		String resultado = "";

		resultado += this.callable + LPAR;

		Iterator iteratorValueClauses = this.valueClauses.iterator();
		Iterator iteratorWhereClauses = this.parameterClauses.iterator();

		while (iteratorValueClauses.hasNext()) {
			MVQLValue value = (MVQLValue) iteratorValueClauses.next();

			resultado += value.generateSQL();
			
			if (iteratorValueClauses.hasNext())
				resultado += COMMA + SP;
		}
		
		resultado += RPAR + SEMICOLON;

		return resultado;
	}

}
