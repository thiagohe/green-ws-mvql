package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class NenhumaConsultaFornecidaException extends Exception {
	public NenhumaConsultaFornecidaException() {
		super(FachadaFramework.getResourceBundle().getString("nenhuma_consulta_fornecida"));
	}
}
