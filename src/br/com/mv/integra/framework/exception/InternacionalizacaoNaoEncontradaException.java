package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class InternacionalizacaoNaoEncontradaException extends Exception {
	public InternacionalizacaoNaoEncontradaException() {
		super(FachadaFramework.getResourceBundle().getString("internacionalizacao_nao_encontrada"));
	}

}
