package br.com.mv.integra.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.mv.integra.repositorio.ObjectDTO;

public class QueryFactory implements IQuery {
	public static final String MVQL_CUSTOM_QUERY = "MVQL:QUERY";
	public static final String MVQL_CALL = "MVQL:CALL";

	public QueryFactory() {

	}

	@Override
	public Query parse(HashMap<String, Object> map, String returnColumn)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Query resultado = null;
		String entity = null;

		for (String key : map.keySet()) {
			entity = key;
		}

		if (entity.equals(MVQL_CUSTOM_QUERY)) {
			String customQuery = map.get(entity).toString();

			resultado = new CustomQuery(customQuery);
		} else if (entity.equals(MVQL_CALL)) {
			String callQuery = map.get(entity).toString();
			
			resultado = CallQuery.parse((HashMap<String, Object>) map.get(entity));
		} else {
			resultado = SelectQuery.parse(map, returnColumn);
		}

		return resultado;
	}

}
