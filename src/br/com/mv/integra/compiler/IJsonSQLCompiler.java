package br.com.mv.integra.compiler;

import br.com.mv.integra.compiler.syntaxtree.SimpleNode;

public interface IJsonSQLCompiler {
	public String select(SimpleNode node);
	public String update(SimpleNode node);
	public String delete(SimpleNode node);
	public String insert(SimpleNode node);
}
