package br.com.mv.integra.query;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.mv.integra.query.MVQLValue.MVQLType;

public class SelectQuery extends Query {
	List<String> selectValues;
	String from;
	List<String> whereClasues;
	List<Object> valueClauses;

	public static SelectQuery parse(HashMap<String, Object> map, String returnColumn) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SelectQuery resultado = null;

		resultado = new SelectQuery();
		resultado.selectValues = new ArrayList<String>();

		for (String key : map.keySet()) {
			resultado.from = key;
		}

		HashMap<String, Object> child = (HashMap<String, Object>) map.get(resultado.from);

		resultado.whereClasues = new ArrayList<String>();
		resultado.valueClauses = new ArrayList<Object>();

		for (String whereClause : child.keySet()) {
			Object childValue = child.get(whereClause);

			if(returnColumn != null) resultado.selectValues.add(returnColumn);
			resultado.whereClasues.add(whereClause);

			if (childValue instanceof HashMap) {
				SelectQuery subquery = SelectQuery.parse((HashMap<String, Object>) childValue, whereClause);

				resultado.valueClauses.add(subquery);
			} else {
				MVQLValue value = new MVQLValue(childValue);
				
				resultado.valueClauses.add(value);
			}

		}

		return resultado;
	}
	
	public String generateSQL() {
		String resultado = "";

		resultado += SELECT + SP;

		Iterator iteratorSelectValues = this.selectValues.iterator();

		if(!iteratorSelectValues.hasNext()) resultado += ALL_COLUMNS + SP;
		
		while (iteratorSelectValues.hasNext()) {
			resultado += iteratorSelectValues.next().toString() + SP;

			if (iteratorSelectValues.hasNext())
				resultado += COMMA + SP;
		}

		resultado += FROM + SP + this.from + SP;

		Iterator iteratorValueClauses = this.valueClauses.iterator();
		Iterator iteratorWhereClauses = this.whereClasues.iterator();

		if (iteratorWhereClauses.hasNext())
			resultado += WHERE + SP;

		while (iteratorValueClauses.hasNext()) {
			Object value = iteratorValueClauses.next();

			if (value instanceof SelectQuery) {
				SelectQuery subquery = (SelectQuery) value;
				resultado += iteratorWhereClauses.next() + SP + IN + SP + LPAR + subquery.generateSQL() + RPAR;
			} else if (value instanceof MVQLValue) {
				if (((MVQLValue) value).type.equals(MVQLType.Date)) {
					resultado += iteratorWhereClauses.next() + SP + GREATER + SP + ((MVQLValue) value).generateSQL() + SP;
				} else if(((MVQLValue) value).type.equals(MVQLType.Null)) {
					resultado += iteratorWhereClauses.next() + SP + IS + SP + ((MVQLValue) value).generateSQL() + SP;
				} else if (((MVQLValue) value).type.equals(MVQLType.String)) {
					resultado += iteratorWhereClauses.next() + SP + LIKE + SP  + ((MVQLValue) value).generateSQL()
							 + SP;
				} else if (((MVQLValue) value).type.equals(MVQLType.Integer)) {
					resultado += iteratorWhereClauses.next() + SP + EQUALS + SP + ((MVQLValue) value).generateSQL() + SP;
				}
			} 
			
			if(iteratorValueClauses.hasNext()) resultado += AND + SP;
		}

		return resultado;
	}

}
