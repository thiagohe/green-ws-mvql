package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class NenhumJSONenviadoException extends Exception {
	public NenhumJSONenviadoException() {
		super(FachadaFramework.getResourceBundle().getString("nenhum_json_enviado"));
	}
}
