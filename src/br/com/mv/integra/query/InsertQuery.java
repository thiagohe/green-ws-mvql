package br.com.mv.integra.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InsertQuery extends Query {
	List<String> insertValues;
	String from;
	List<Object> valueClauses;
	
	public InsertQuery parse(HashMap<String, Object> map, String returnColumn) throws JsonParseException, JsonMappingException, IOException {
		InsertQuery resultado = null;

		resultado = new InsertQuery();

		for (String key : map.keySet()) {
			resultado.from = key;
		}

		HashMap<String, Object> child = (HashMap<String, Object>) map.get(resultado.from);

		resultado.insertValues = new ArrayList<String>();
		resultado.valueClauses = new ArrayList<Object>();

		for (String insertValue : child.keySet()) {
			Object valueClause = child.get(insertValue);
			resultado.insertValues.add(insertValue);
			resultado.valueClauses.add(valueClause);
		}
		
		String schemaName = resultado.from.substring(0, resultado.from.indexOf("."));
		String tableName = resultado.from.substring(resultado.from.indexOf(".") + 1, resultado.from.length());
		String sequenceName = SEQ + tableName;
		String query = "";
		
		resultado.insertValues.add(CD + tableName);
		
		
		query = sequenceName + NEXTVAL;
		ReservedCommandsQuery reservedCommandQuery = new ReservedCommandsQuery(query);
		
		resultado.valueClauses.add(reservedCommandQuery);

		return resultado;
	}
	
	public String generateSQL() {
		String resultado = "";

		resultado += INSERT + SP + INTO + SP + this.from + SP;

		Iterator iteratorInsertValues = this.insertValues.iterator();

		if(iteratorInsertValues.hasNext()) resultado += LPAR + SP;
		
		while (iteratorInsertValues.hasNext()) {
			resultado += iteratorInsertValues.next().toString() + SP;

			if (iteratorInsertValues.hasNext())
				resultado += COMMA + SP;
		}

		resultado += RPAR + SP;
		
		resultado += VALUES + SP;
		
		Iterator iteratorValueClauses = this.valueClauses.iterator();

		if (iteratorValueClauses.hasNext())
			resultado += LPAR + SP;

		while (iteratorValueClauses.hasNext()) {
			Object value = iteratorValueClauses.next();
			
			resultado += this.toValue(value);
			
			if (iteratorValueClauses.hasNext())
				resultado += COMMA + SP;
		}
		
		resultado += RPAR;

		return resultado;
	}
	
	public String toValue(Object value) {
		String resultado = "";
		
		if (value instanceof String) {
			String date = null, time = null;
			Scanner scanner = new Scanner(value.toString());
			try {
				date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
			} catch (java.util.InputMismatchException e) {
			}

			if (date != null) {
				date = value.toString().substring(0, value.toString().length() - 2);
				resultado = TO_DATE + LPAR + QUOTE + date + QUOTE + COMMA + QUOTE
						+ DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR;
			} else {
				resultado = QUOTE + value.toString() + QUOTE;
			}

		} else if (value instanceof Integer) {
			resultado = value.toString();
		} else if (value instanceof ReservedCommandsQuery) {
			resultado = ((ReservedCommandsQuery) value).generateSQL();
		}

		return resultado;
	}

}
