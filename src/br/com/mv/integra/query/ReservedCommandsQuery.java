package br.com.mv.integra.query;

public class ReservedCommandsQuery {
	public String query;
	
	public ReservedCommandsQuery(String query) {
		this.query = query;
	}
	
	public String generateSQL() {
		return this.query;
	}
}
