package br.com.mv.integra.query;

import oracle.jdbc.OracleTypes;

public class MVQLParameterType {
	public enum MVQLParameterDatumType {Number, Varchar2, Float, Date, DefinedByValue}

	private static final String VARCHAR2 = "VARCHAR2";

	private static final String NUMBER = "NUMBER";

	private static final String FLOAT = "FLOAT";

	private static final String DATE = "DATE";
	
	public String parameterName;
	public String parameterType;
	public MVQLParameterDatumType datumType;
	public int oracleType;
	
	public MVQLParameterType(String parameter) {
		String[] params = parameter.split("[.]");
		
		if(params.length > 2) {
			this.parameterType = params[0];
			this.parameterName = params[1];
			this.setDatumType(params[2]);
		} else if(params.length > 1) {
			this.parameterType = params[0];
			this.parameterName = params[1];
			this.datumType = MVQLParameterDatumType.DefinedByValue;
		}
	}

	private void setDatumType(String declaredType) {
		if(declaredType.equalsIgnoreCase(VARCHAR2)) {
			this.datumType = MVQLParameterDatumType.Varchar2;
			this.oracleType = OracleTypes.VARCHAR;
		} else if(declaredType.equalsIgnoreCase(NUMBER)) {
			this.datumType = MVQLParameterDatumType.Number;
			this.oracleType = OracleTypes.NUMBER;
		} else if(declaredType.equalsIgnoreCase(FLOAT)) {
			this.datumType = MVQLParameterDatumType.Float;
			this.oracleType = OracleTypes.FLOAT;
		} else if(declaredType.equalsIgnoreCase(DATE)) {
			this.datumType = MVQLParameterDatumType.Date;
			this.oracleType = OracleTypes.TIMESTAMP;
		}
	}
}
