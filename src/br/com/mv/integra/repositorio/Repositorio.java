package br.com.mv.integra.repositorio;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.mv.integra.framework.FachadaFramework;
import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;
import br.com.mv.integra.framework.exception.NenhumRegistroEncontradoException;
import br.com.mv.integra.framework.exception.NenhumRegistroInseridoException;
import br.com.mv.integra.framework.exception.NenhumaConsultaFornecidaException;
import br.com.mv.integra.framework.exception.TipoCallableIndefinidoException;
import br.com.mv.integra.query.CallQuery;
import br.com.mv.integra.query.DeleteQuery;
import br.com.mv.integra.query.InsertQuery;
import br.com.mv.integra.query.JdbcCallFactory;
import br.com.mv.integra.query.MVQLJdbcCall;
import br.com.mv.integra.query.MVQLParameterType;
import br.com.mv.integra.query.Query;
import br.com.mv.integra.query.QueryFactory;
import br.com.mv.integra.query.UpdateQuery;
import br.com.mv.integra.utils.DTOUtils;
import oracle.jdbc.OracleTypes;

@Repository
public class Repositorio {
	private static final String BEGIN = "BEGIN";
	private static final String ATRIBUI_EMPRESA = "dbamv.pkg_mv2000.atribui_empresa(1)";
	private static final String END = "END";
	private static final String SEMICOLON = ";";
	private static final String SP = " ";	
	
    public static JdbcTemplate jdbcTemplate;
    public static QueryFactory queryFactory; 
	
	@Autowired
	public Repositorio(JdbcTemplate jdbcTemplate) {
		Repositorio.jdbcTemplate = jdbcTemplate;
		Repositorio.queryFactory = new QueryFactory(); 
	}
	
	public Repositorio() {
		
	}
	
	public static JdbcTemplate getJDBCTemplate() {
		return Repositorio.jdbcTemplate;
	}
	
	public List<ObjectDTO> select(ObjectDTO objectDTO) throws NenhumRegistroEncontradoException, NenhumaConsultaFornecidaException, ChaveNaoExistenteException, JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String query = null;
		ObjectDTOMapper mapper = new ObjectDTOMapper();
		List<ObjectDTO> resultado = null;
		
		query = Repositorio.queryFactory.parse(objectDTO.getMap(), null).generateSQL();
		
		FachadaFramework.logger.debug("Consulta gerada a partir do JSON: "+query);
		
		if(query == null) {
			throw new NenhumaConsultaFornecidaException();
		}
				
		resultado = (List<ObjectDTO>) Repositorio.getJDBCTemplate().query(query, mapper);
		
		FachadaFramework.logger.debug("Consulta realizada com sucesso.");
		
		if(resultado == null) {
			throw new NenhumRegistroEncontradoException();
		}
		
		return resultado;
	}

	public int insert(ObjectDTO objectDTO) throws NenhumaConsultaFornecidaException, NenhumRegistroEncontradoException, NenhumRegistroInseridoException, JsonParseException, JsonMappingException, IOException {
		String query = null;
		ObjectDTOMapper mapper = new ObjectDTOMapper();
		int resultado = -1;
		
		query = (new InsertQuery().parse(objectDTO.getMap(), null)).generateSQL();
		
		if(query == null) {
			throw new NenhumaConsultaFornecidaException();
		}
		
		String block = BEGIN + SP;
		block += ATRIBUI_EMPRESA + SEMICOLON + SP;
		block += query + SEMICOLON + SP;
		block += END + SEMICOLON;
		
		resultado = Repositorio.getJDBCTemplate().update(block);
		
		if(resultado == -1) {	
			throw new NenhumRegistroInseridoException();
		}
		
		return resultado;
	}

	public int update(ObjectDTO objectDTO) throws NenhumaConsultaFornecidaException, NenhumRegistroInseridoException, JsonParseException, JsonMappingException, IOException {
		String query = null;
		ObjectDTOMapper mapper = new ObjectDTOMapper();
		int resultado = -1;
		
		query = (new UpdateQuery().parse(objectDTO.getMap(), null)).generateSQL();
		
		if(query == null) {
			throw new NenhumaConsultaFornecidaException();
		}
		
		String block = BEGIN + SP;
		block += ATRIBUI_EMPRESA + SEMICOLON + SP;
		block += query + SEMICOLON + SP;
		block += END + SEMICOLON;
		
		resultado = Repositorio.getJDBCTemplate().update(block);
		
		if(resultado == -1) {	
			throw new NenhumRegistroInseridoException();
		}
		
		return resultado;
	}

	public int delete(ObjectDTO objectDTO) throws NenhumaConsultaFornecidaException, NenhumRegistroInseridoException, JsonParseException, JsonMappingException, IOException {
		String query = null;
		ObjectDTOMapper mapper = new ObjectDTOMapper();
		int resultado = -1;
		
		query = (new DeleteQuery().parse(objectDTO.getMap(), null)).generateSQL();
		
		if(query == null) {
			throw new NenhumaConsultaFornecidaException();
		}
		
		String block = BEGIN + SP;
		block += ATRIBUI_EMPRESA + SEMICOLON + SP;
		block += query + SEMICOLON + SP;
		block += END + SEMICOLON;
		
		resultado = Repositorio.getJDBCTemplate().update(block);
		
		if(resultado == -1) {	
			throw new NenhumRegistroInseridoException();
		}
		
		return resultado;
	}

	public List<ObjectDTO> call(ObjectDTO objectDTO) throws NenhumRegistroEncontradoException, NenhumaConsultaFornecidaException, ChaveNaoExistenteException, JsonParseException, JsonMappingException, JsonProcessingException, IOException, TipoCallableIndefinidoException, ParseException {
		String query = null;
		ObjectDTOMapper mapper = new ObjectDTOMapper();
		List<ObjectDTO> resultado = new ArrayList<ObjectDTO>();
		
		CallQuery callQuery = (CallQuery) Repositorio.queryFactory.parse(objectDTO.getMap(), null);
		query = callQuery.generateSQL();
		
		FachadaFramework.logger.debug("Consulta gerada a partir do JSON: "+query);
		
		if(query == null) {
			throw new NenhumaConsultaFornecidaException();
		}
				
		MVQLJdbcCall jdbcCall = JdbcCallFactory.create(callQuery);

		HashMap<String,Object> resultadoCall;
		resultadoCall = (HashMap<String, Object>) jdbcCall.execute();
		ObjectDTO dto = DTOUtils.mapToObjectDTO(resultadoCall);
		resultado.add(dto);
		
		FachadaFramework.logger.debug("Consulta realizada com sucesso.");
		
		return resultado;
	}
	
	
	
}
