package br.com.mv.integra.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mv.integra.framework.FachadaFramework;
import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;
import br.com.mv.integra.framework.exception.NenhumaConsultaFornecidaException;
import br.com.mv.integra.query.Subquery;
import br.com.mv.integra.repositorio.ObjectDTO;

public class DTOUtils {
	public static final String SELECT = "SELECT";
	public static final String ALL_COLUMNS = "*";
	public static final String FROM = "FROM";
	public static final String WHERE = "WHERE";
	public static final String EQUALS = "=";
	public static final String LIKE = "LIKE";
	public static final String LIKE_TOKEN = "%";
	public static final String QUOTE = "'";
	public static final String SP = " ";
	public static final String AND = "AND";
	public static final String COMMA = ",";
	private static final String LPAR = "(";
	private static final String TO_DATE = "TO_DATE";
	private static final String DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS = "YYYY-MM-DD HH24:MI:SS";
	private static final String RPAR = ")";
	private static final String GREATER = ">";
	private static final String IN = "IN";

	public static String dtoToSQLSelect(ObjectDTO objectDTO)
			throws NenhumaConsultaFornecidaException, ChaveNaoExistenteException, JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String resultado = "";
		String tableName = null;
		List<String> columns = new ArrayList<String>();
		List<Object> values = new ArrayList<Object>();

		for (String key : objectDTO.getMap().keySet()) {
			tableName = key;
		}

		HashMap<String,Object> entity = (HashMap<String,Object>) objectDTO.get(tableName);

		for (String column : entity.keySet()) {
			columns.add(column);
			values.add(entity.get(column));
		}

		resultado += SELECT;
		resultado += SP;

		resultado += ALL_COLUMNS;

		resultado += SP;

		resultado += FROM;

		resultado += SP;

		resultado += tableName;

		resultado += SP;

		resultado += WHERE;

		resultado += SP;

		for (int i = 0; i < values.size(); i++) {
			Object value = values.get(i);
			String column = columns.get(i);

			if (value instanceof String) {
				String date = null, time = null;
				Scanner scanner = new Scanner(value.toString());
				try {
					date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
				} catch (java.util.InputMismatchException e) {
				}

				if (date != null) {
					date = value.toString().substring(0, value.toString().length() - 2);
					resultado += column + SP + GREATER + SP + TO_DATE + LPAR + QUOTE + date + QUOTE + COMMA + QUOTE
							+ DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR;
				} else {
					resultado += column + SP + LIKE + SP + QUOTE + LIKE_TOKEN + value.toString() + LIKE_TOKEN + QUOTE;
				}

			} else if (value instanceof Integer) {
				resultado += SP + column + SP + EQUALS + SP + new Integer(value.toString());
			}

			resultado += SP + AND + SP;

		}

		resultado = resultado.substring(0, resultado.length() - 5);

		if (resultado == null) {
			throw new NenhumaConsultaFornecidaException();
		}

		return resultado;
	}

	public static Object toSQLSelect(HashMap<String, Object> map, String parent, String selectedColumn) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String resultado = "";
		String tableName = "";
		List<String> columns = new ArrayList<String>();
		List<Object> values = new ArrayList<Object>();
		List<String> clauses = new ArrayList<String>();

		for (String column : map.keySet()) {
			Object value = map.get(column);
			
			if(parent == null) tableName = column;

			if (value instanceof HashMap) {
				String query = null;

				if (parent == null && selectedColumn == null) {
					query = DTOUtils.toSQLSelect((HashMap<String, Object>) value, tableName, null).toString();
				} else if(parent != null && !column.contains(".")) {
					query = DTOUtils.toSQLSelect((HashMap<String, Object>) value, parent, column).toString();
				} else if(parent != null && column.contains(".")) {
					return DTOUtils.toSQLSelect((HashMap<String, Object>) value, column, null).toString();
				}  

				Subquery subquery = new Subquery(query);
				
				if(selectedColumn != null) {
					columns.add(selectedColumn);
				} else {
					columns.add(column);
				}
				values.add(subquery);

			} else {
				columns.add(column);
				values.add(value);
			}
		}

		for (int i = 0; i < values.size(); i++) {
			String clause = "";
			String column = columns.get(i);
			Object value = values.get(i);

			if (value instanceof Subquery) {
				clause = column + SP + IN + SP + value + SP;
			} else if (value instanceof String) {
				String date = null, time = null;
				Scanner scanner = new Scanner(value.toString());
				try {
					date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
				} catch (java.util.InputMismatchException e) {
				}

				if (date != null) {
					date = value.toString().substring(0, value.toString().length() - 2);
					clause = column + SP + GREATER + SP + TO_DATE + LPAR + QUOTE + date + QUOTE + COMMA + QUOTE
							+ DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR + SP;
				} else {
					clause = column + SP + LIKE + SP + QUOTE + LIKE_TOKEN + value.toString() + LIKE_TOKEN + QUOTE + SP;
				}
			} else if (value instanceof Integer) {
				clause = column + SP + EQUALS + SP + new Integer(value.toString()) + SP;
			}

			clauses.add(clause);
		}

		if (parent == null) {
			resultado = SELECT + SP + ALL_COLUMNS + SP + FROM + SP + tableName + SP;

			Iterator iterator = clauses.iterator();

			if (iterator.hasNext())
				resultado += WHERE + SP;

			while (iterator.hasNext()) {
				resultado += iterator.next().toString();

				if (iterator.hasNext())
					resultado += AND + SP;
			}
			resultado += RPAR;
		} else if (selectedColumn == null) {
			resultado = LPAR + SELECT + SP + ALL_COLUMNS + SP + FROM + SP + parent + SP;

			Iterator iterator = clauses.iterator();

			if (iterator.hasNext())
				resultado += WHERE + SP;

			while (iterator.hasNext()) {
				resultado += iterator.next().toString();

				if (iterator.hasNext())
					resultado += AND + SP;
			}
			resultado += RPAR;
		} else {
			resultado = LPAR + SELECT + SP + selectedColumn + SP + FROM + SP + parent + SP;

			Iterator iterator = clauses.iterator();

			if (iterator.hasNext())
				resultado += WHERE + SP;

			while (iterator.hasNext()) {
				resultado += iterator.next().toString();

				if (iterator.hasNext())
					resultado += AND + SP;
			}
			resultado += RPAR;
		}

		return resultado;
	}

	public static ObjectDTO mapToObjectDTO(Map<String, Object> resultadoCall) {
		ObjectDTO resultado = new ObjectDTO();
		
		Iterator<String> iteratorKey = resultadoCall.keySet().iterator();

		while(iteratorKey.hasNext()) {
			String key = iteratorKey.next();
			Object value = resultadoCall.get(key);
			resultado.add(key, value);
		}
		
		return resultado;
	}
	
}
