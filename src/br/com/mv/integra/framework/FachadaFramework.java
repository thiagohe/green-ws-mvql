package br.com.mv.integra.framework;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mv.integra.framework.exception.InternacionalizacaoNaoEncontradaException;
import br.com.mv.integra.framework.exception.VersaoInvalidaException;

public class FachadaFramework {
	public static final String APPLICATION_MESSAGES = "ApplicationMessages";
	public static final int V_001 = 1;
	public static Logger logger;
	
	private static ResourceBundle resourceBundle;
	private static IJIntegracaoFramework iJintegracaoFramework;
	
	public FachadaFramework(int version, Locale locale, Logger logger) throws InternacionalizacaoNaoEncontradaException, VersaoInvalidaException {
		if(FachadaFramework.iJintegracaoFramework == null) FachadaFramework.iJintegracaoFramework = FactoryJIntegracaoFramework.create(version);
		if(FachadaFramework.resourceBundle == null) FachadaFramework.resourceBundle = ResourceBundle.getBundle(FachadaFramework.APPLICATION_MESSAGES, locale);
		if(FachadaFramework.logger == null) FachadaFramework.logger = logger;
	}
	
	public static IJIntegracaoFramework getFramework() throws VersaoInvalidaException {		
		return FachadaFramework.iJintegracaoFramework;
	}

	public static ResourceBundle getResourceBundle() {
		return FachadaFramework.resourceBundle;
	}
}
