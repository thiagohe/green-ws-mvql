package br.com.mv.integra.framework;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mv.integra.compiler.parser.ParseException;
import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;
import br.com.mv.integra.framework.exception.NenhumJSONenviadoException;
import br.com.mv.integra.framework.exception.NenhumRegistroEncontradoException;
import br.com.mv.integra.framework.exception.NenhumaConsultaFornecidaException;
import br.com.mv.integra.framework.exception.TipoCallableIndefinidoException;
import br.com.mv.integra.framework.exception.VersaoInvalidaException;

public interface IJIntegracaoFramework {
	public String find(String parameter); 	
//	public String create(String parameter);
//	public String update(String parameter);
//	public String remove(String parameter);
	public String call(String parameter);
	public ObjectMapper getObjectMapper();
}
