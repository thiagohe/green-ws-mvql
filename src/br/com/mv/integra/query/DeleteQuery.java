package br.com.mv.integra.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class DeleteQuery extends Query {
	String from;
	List<String> whereClauses;
	List<Object> valueClauses;
	
	public DeleteQuery parse(HashMap<String, Object> map, String returnColumn) throws JsonParseException, JsonMappingException, IOException {
		DeleteQuery resultado = null;

		resultado = new DeleteQuery();

		for (String key : map.keySet()) {
			resultado.from = key;
		}

		HashMap<String, Object> child = (HashMap<String, Object>) map.get(resultado.from);

		resultado.whereClauses = new ArrayList<String>();
		resultado.valueClauses = new ArrayList<Object>();

		for (String whereClause : child.keySet()) {
			Object valueClause = child.get(whereClause);
			
			resultado.whereClauses.add(whereClause);
			resultado.valueClauses.add(valueClause);
		}

		return resultado;
	}
	
	public String generateSQL() {
		String resultado = "";

		resultado += DELETE + SP + FROM + SP + this.from + SP;
		
		Iterator iteratorWhereClauses = this.whereClauses.iterator();
		Iterator iteratorValueClauses = this.valueClauses.iterator();
		
		if(iteratorWhereClauses.hasNext()) resultado += WHERE + SP;
		
		while(iteratorValueClauses.hasNext()) {
			Object value = iteratorValueClauses.next();
			
			if (value instanceof String) {
				String date = null, time = null;
				Scanner scanner = new Scanner(value.toString());
				try {
					date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
				} catch (java.util.InputMismatchException e) {
				}

				if (date != null) {
					date = value.toString().substring(0, value.toString().length() - 2);
					resultado += iteratorWhereClauses.next() + SP + GREATER + SP + TO_DATE + LPAR + QUOTE + date + QUOTE
							+ COMMA + QUOTE + DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR + SP;
				} else {
					resultado += iteratorWhereClauses.next() + SP + LIKE + SP + QUOTE + LIKE_TOKEN + value.toString()
							+ LIKE_TOKEN + QUOTE + SP;
				}
			} else if (value instanceof Integer) {
				resultado += iteratorWhereClauses.next() + SP + EQUALS + SP + new Integer(value.toString()) + SP;
			}
			
			if (iteratorWhereClauses.hasNext())
				resultado += COMMA + SP;
		}
		
		return resultado;
	}
	
	public String toValue(Object value) {
		String resultado = "";
		
		if (value instanceof String) {
			String date = null, time = null;
			Scanner scanner = new Scanner(value.toString());
			try {
				date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
			} catch (java.util.InputMismatchException e) {
			}

			if (date != null) {
				date = value.toString().substring(0, value.toString().length() - 2);
				resultado = TO_DATE + LPAR + QUOTE + date + QUOTE + COMMA + QUOTE
						+ DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR;
			} else {
				resultado = QUOTE + value.toString() + QUOTE;
			}

		} else if (value instanceof Integer) {
			resultado = value.toString();
		} else if (value instanceof ReservedCommandsQuery) {
			resultado = ((ReservedCommandsQuery) value).generateSQL();
		}

		return resultado;
	}

}
