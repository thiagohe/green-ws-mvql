package br.com.mv.integra.compiler;

import br.com.mv.integra.compiler.parser.JSONParserVisitor;
import br.com.mv.integra.compiler.syntaxtree.JsonSQLAST;
import br.com.mv.integra.compiler.syntaxtree.SimpleNode;

public class IJsonSQLCompilerImpl implements IJsonSQLCompiler {
	JsonSQLAST ast;
	
	public IJsonSQLCompilerImpl(SimpleNode root) {
		this.ast = new JsonSQLAST(root);
		this.ast.start();
	}
	
	public String select(SimpleNode node) {
		return this.ast.toString();
	}

	public String update(SimpleNode node) {
		// TODO Auto-generated method stub
		return null;
	}

	public String delete(SimpleNode node) {
		// TODO Auto-generated method stub
		return null;
	}

	public String insert(SimpleNode node) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
