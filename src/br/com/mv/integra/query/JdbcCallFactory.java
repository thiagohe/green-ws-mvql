package br.com.mv.integra.query;

import java.sql.Date;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;

import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import br.com.mv.integra.framework.exception.TipoCallableIndefinidoException;
import br.com.mv.integra.query.CallQuery.CallQueryType;
import br.com.mv.integra.query.MVQLValue.MVQLType;
import br.com.mv.integra.repositorio.Repositorio;
import oracle.jdbc.OracleTypes;

public class JdbcCallFactory {
	private static final String MVQL_IN = "MVQL:IN";
	private static final String MVQL_OUT = "MVQL:OUT";
	private static final String MVQL_INOUT = "MVQL:INOUT";
	
	public static MVQLJdbcCall create(CallQuery callQuery) throws TipoCallableIndefinidoException, ParseException {
		MVQLJdbcCall jdbcCall = null;

		if (callQuery.callableType.equals(Query.FUNCTION)) {
			if(callQuery.type.equals(CallQueryType.Simple)) {
				jdbcCall = new MVQLJdbcCall();
				jdbcCall.withSchemaName(callQuery.schema)
				.withFunctionName(callQuery.callable);				
			} else if(callQuery.type.equals(CallQueryType.Complete)) {
				jdbcCall = new MVQLJdbcCall();			
				jdbcCall.withSchemaName(callQuery.schema)				
				.withCatalogName(callQuery.packaging)
				.withFunctionName(callQuery.callable);
			}
		} else if (callQuery.callableType.equals(Query.PROCEDURE)) {
			if(callQuery.type.equals(CallQueryType.Simple)) {
				jdbcCall = new MVQLJdbcCall();
				jdbcCall.withSchemaName(callQuery.schema)
				.withProcedureName(callQuery.callable);
			} else if(callQuery.type.equals(CallQueryType.Complete)) {
				jdbcCall = new MVQLJdbcCall();
				jdbcCall.withSchemaName(callQuery.schema)
				.withCatalogName(callQuery.packaging)
				.withProcedureName(callQuery.callable);
			}
		}

		if (jdbcCall == null)
			throw new TipoCallableIndefinidoException();

		Iterator<MVQLParameterType> iteratorParameters = callQuery.parameterClauses.iterator();
		Iterator<MVQLValue> iteratorValues = callQuery.valueClauses.iterator();
		
		while(iteratorValues.hasNext()) {
			MVQLValue value = iteratorValues.next();
			MVQLParameterType parameter = iteratorParameters.next();
			
			if(parameter.parameterType.equals(MVQL_IN)) {
				if(value.type.equals(MVQLType.Date)) {
					jdbcCall.addDeclaredParameter(new SqlParameter(parameter.parameterName, OracleTypes.TIMESTAMP));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.TIMESTAMP);
				} else if (value.type.equals(MVQLType.String)) {
					jdbcCall.addDeclaredParameter(new SqlParameter(parameter.parameterName, OracleTypes.VARCHAR));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.VARCHAR);
				} else if (value.type.equals(MVQLType.Integer)) {
					jdbcCall.addDeclaredParameter(new SqlParameter(parameter.parameterName, OracleTypes.NUMBER));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.NUMBER);
				} else if (value.type.equals(MVQLType.Double)) {
					jdbcCall.addDeclaredParameter(new SqlParameter(parameter.parameterName, OracleTypes.DOUBLE));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.DOUBLE);
				} else {
					jdbcCall.addDeclaredParameter(new SqlParameter(parameter.parameterName, parameter.oracleType));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), parameter.oracleType);
				}
			} else if(parameter.parameterType.equals(MVQL_INOUT)) {
				if(value.type.equals(MVQLType.Date)) {					
					jdbcCall.addDeclaredParameter(new SqlInOutParameter(parameter.parameterName, OracleTypes.TIMESTAMP));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.TIMESTAMP);
				} else if (value.type.equals(MVQLType.String)) {
					jdbcCall.addDeclaredParameter(new SqlInOutParameter(parameter.parameterName, OracleTypes.VARCHAR));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.VARCHAR);
				} else if (value.type.equals(MVQLType.Integer)) {
					jdbcCall.addDeclaredParameter(new SqlInOutParameter(parameter.parameterName, OracleTypes.NUMBER));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.NUMBER);
				} else if (value.type.equals(MVQLType.Double)) {
					jdbcCall.addDeclaredParameter(new SqlInOutParameter(parameter.parameterName, OracleTypes.DOUBLE));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), OracleTypes.DOUBLE);
				} else {
					jdbcCall.addDeclaredParameter(new SqlInOutParameter(parameter.parameterName, parameter.oracleType));
					jdbcCall.params.addValue(parameter.parameterName, value.toValue(), parameter.oracleType);
				}
				
			} else if(parameter.parameterType.equals(MVQL_OUT)) {
				if(value.type.equals(MVQLType.Date)) {
					jdbcCall.addDeclaredParameter(new SqlOutParameter(parameter.parameterName, OracleTypes.TIMESTAMP));
				} else if (value.type.equals(MVQLType.String)) {
					jdbcCall.addDeclaredParameter(new SqlOutParameter(parameter.parameterName, OracleTypes.VARCHAR));
				} else if (value.type.equals(MVQLType.Integer)) {
					jdbcCall.addDeclaredParameter(new SqlOutParameter(parameter.parameterName, OracleTypes.NUMBER));
				} else if (value.type.equals(MVQLType.Double)) {
					jdbcCall.addDeclaredParameter(new SqlOutParameter(parameter.parameterName, OracleTypes.DOUBLE));
				} else {
					jdbcCall.addDeclaredParameter(new SqlOutParameter(parameter.parameterName, parameter.oracleType));
				}
				
			}
		}
		
		return jdbcCall;
	}

}
