package br.com.mv.integra.repositorio;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;

import org.springframework.jdbc.core.RowMapper;

public class ObjectDTOMapper implements RowMapper {
	
	
	private static final String ENTITY = "ENTITY_NAME";

	public ObjectDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ObjectDTO retorno = new ObjectDTO();
		
		ResultSetMetaData resultSetMetadata = rs.getMetaData();
		
		int numColumns = resultSetMetadata.getColumnCount();
		
		String entityName = ENTITY;
		
		
		
		for(int columnIndex = 1; columnIndex <= numColumns; columnIndex++) {
			Object value = rs.getObject(columnIndex);
			String columnName = resultSetMetadata.getColumnName(columnIndex);
			
			retorno.add(columnName, value);			
		}			
						
		return retorno;
	}
}
