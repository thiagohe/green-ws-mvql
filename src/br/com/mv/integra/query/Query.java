package br.com.mv.integra.query;

import java.util.HashMap;

import br.com.mv.integra.repositorio.ObjectDTO;

public abstract class Query {
	public static final String UPDATE = "UPDATE";
	public static final String SET = "SET";
	public static final String ALL_COLUMNS = "*";
	public static final String FROM = "FROM";
	public static final String WHERE = "WHERE";
	public static final String EQUALS = "=";
	public static final String LIKE = "LIKE";
	public static final String LIKE_TOKEN = "%";
	public static final String QUOTE = "'";
	public static final String SP = " ";
	public static final String AND = "AND";
	public static final String COMMA = ",";
	public static final String LPAR = "(";
	public static final String TO_DATE = "TO_DATE";
	public static final String DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS = "YYYY-MM-DD HH24:MI:SS";
	public static final String RPAR = ")";
	public static final String GREATER = ">";
	public static final String IN = "IN";
	public static final String INTO = "INTO";
	public static final String INSERT = "INSERT";
	public static final String VALUES = "VALUES";
	public static final String CD = "cd_";
	public static final String SEQ = "seq_";
	public static final String DUAL = "DUAL";
	public static final String NEXTVAL = ".NEXTVAL";
	public static final String DELETE = "DELETE";
	public static final String SELECT = "SELECT";
	public static final String NULL = "null";
	public static final Object IS = "IS";
	public static final String SEMICOLON = ";";
	public static final String FUNCTION = "FNC";
	public static final String PROCEDURE = "PRC";
	
	public abstract String generateSQL();
}
