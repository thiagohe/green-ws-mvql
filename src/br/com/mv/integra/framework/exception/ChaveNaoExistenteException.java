package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class ChaveNaoExistenteException extends Exception {
	public ChaveNaoExistenteException() {
		super(FachadaFramework.getResourceBundle().getString("chave_nao_existente"));
	}

}
