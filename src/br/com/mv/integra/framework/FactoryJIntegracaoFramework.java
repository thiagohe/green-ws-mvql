package br.com.mv.integra.framework;

import br.com.mv.integra.framework.exception.VersaoInvalidaException;

public class FactoryJIntegracaoFramework {
	public static final int v001 = 1;

	public FactoryJIntegracaoFramework() {
		
	}

	public static IJIntegracaoFramework create(int version) throws VersaoInvalidaException {
		IJIntegracaoFramework resultado = null;
		
		switch(version) {
			case v001:
				resultado = new IJIntegracaoFramework001Impl(version);
			break;
		}
		
		if(resultado == null) {
			throw new VersaoInvalidaException();
		}
		
		return resultado;
	}
}
