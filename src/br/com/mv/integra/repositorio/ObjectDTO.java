package br.com.mv.integra.repositorio;

import java.util.HashMap;

import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;

public class ObjectDTO {
	public static final String ENTITY_NAME = "ENTITY_NAME";
	HashMap<String, Object> map;
	
	public ObjectDTO() {
		this.map = new HashMap<String, Object>();
		this.map.put(ENTITY_NAME, new HashMap<String, Object>());
	}
	
	public ObjectDTO(HashMap<String, Object> map) {
		this.map = map;
	}
	
	public void add(String key, Object value) {
		((HashMap<String,Object>) this.map.get(ENTITY_NAME)).put(key, value);
	}
	
	public void remove(String key) {
		((HashMap<String,Object>) this.map.get(ENTITY_NAME)).remove(key);
	}
	
	public String getEntity() {
		String resultado = null;
		
		for(String key : this.map.keySet()) {
			resultado = key;
		}
		
		return resultado;
	}
	
	public Object get(String key) throws ChaveNaoExistenteException {
		Object resultado = null;
		
		resultado = this.map.get(key);
		
		if(resultado == null) throw new ChaveNaoExistenteException();
		
		return resultado;		
	}

	public HashMap<String, Object> getMap() {
		return this.map;
	}
}
