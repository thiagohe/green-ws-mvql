package br.com.mv.integra.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;
import br.com.mv.integra.framework.exception.NenhumRegistroEncontradoException;
import br.com.mv.integra.framework.exception.NenhumRegistroInseridoException;
import br.com.mv.integra.framework.exception.NenhumaConsultaFornecidaException;
import br.com.mv.integra.framework.exception.TipoCallableIndefinidoException;
import br.com.mv.integra.repositorio.ObjectDTO;
import br.com.mv.integra.repositorio.Repositorio;

public class IFrameworkController001Impl implements IFrameworkController {
	private Repositorio repositorio;
	
	public IFrameworkController001Impl() {
		this.repositorio = new Repositorio();
	}
	
	public int create(ObjectDTO objectDTO) throws NenhumaConsultaFornecidaException, NenhumRegistroEncontradoException, NenhumRegistroInseridoException, JsonParseException, JsonMappingException, IOException {
		return this.repositorio.insert(objectDTO);
	}

	public int update(ObjectDTO objectDTO) throws NenhumaConsultaFornecidaException, NenhumRegistroInseridoException, JsonParseException, JsonMappingException, IOException {
		return this.repositorio.update(objectDTO);
	}

	public int delete(ObjectDTO objectDTO) throws NenhumaConsultaFornecidaException, NenhumRegistroInseridoException, JsonParseException, JsonMappingException, IOException {
		return this.repositorio.delete(objectDTO);
	}

	public List<ObjectDTO> list(ObjectDTO objectDTO) throws NenhumRegistroEncontradoException, NenhumaConsultaFornecidaException, ChaveNaoExistenteException, JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return this.repositorio.select(objectDTO);
	}

	public List<ObjectDTO> call(ObjectDTO objectDTO)  throws NenhumRegistroEncontradoException, NenhumaConsultaFornecidaException, ChaveNaoExistenteException, JsonParseException, JsonMappingException, JsonProcessingException, IOException, TipoCallableIndefinidoException, ParseException {
		return this.repositorio.call(objectDTO);
	}

	

	
}
