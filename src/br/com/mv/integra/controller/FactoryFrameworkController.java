package br.com.mv.integra.controller;

import br.com.mv.integra.framework.exception.VersaoInvalidaException;

public class FactoryFrameworkController {
	public static final int v001 = 1;

	public static IFrameworkController create(int version) throws VersaoInvalidaException {
		IFrameworkController resultado = null;

		switch (version) {
		case v001:
			resultado = new IFrameworkController001Impl();
			break;
		}

		if (resultado == null) {
			throw new VersaoInvalidaException();
		}

		return resultado;
	}
}
