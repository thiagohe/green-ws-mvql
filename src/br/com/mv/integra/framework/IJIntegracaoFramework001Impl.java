package br.com.mv.integra.framework;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mv.integra.compiler.parser.JSONParser;
import br.com.mv.integra.compiler.parser.ParseException;
import br.com.mv.integra.controller.FactoryFrameworkController;
import br.com.mv.integra.controller.IFrameworkController;
import br.com.mv.integra.framework.exception.ChaveNaoExistenteException;
import br.com.mv.integra.framework.exception.MVQLException;
import br.com.mv.integra.framework.exception.NenhumJSONenviadoException;
import br.com.mv.integra.framework.exception.NenhumRegistroEncontradoException;
import br.com.mv.integra.framework.exception.NenhumRegistroInseridoException;
import br.com.mv.integra.framework.exception.NenhumaConsultaFornecidaException;
import br.com.mv.integra.framework.exception.TipoCallableIndefinidoException;
import br.com.mv.integra.framework.exception.VersaoInvalidaException;
import br.com.mv.integra.repositorio.ObjectDTO;
import br.com.mv.integra.utils.JSONUtils;

public class IJIntegracaoFramework001Impl implements IJIntegracaoFramework {
	IFrameworkController iFrameworkController;
	ObjectMapper objectMapper;

	public IJIntegracaoFramework001Impl(int versao) throws VersaoInvalidaException {
		this.iFrameworkController = FactoryFrameworkController.create(versao);

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.objectMapper = new ObjectMapper();
		this.objectMapper.setDateFormat(df);
	}

	public ObjectMapper getObjectMapper() {
		return this.objectMapper;
	}

	public String find(String parameter) {
		String resultado = null;
		HashMap<String, Object> map;
		ObjectDTO objectDTOEnviado = null;
		List<ObjectDTO> listObjectDTORecebido = null;
		List<String> elementos = new ArrayList<String>();
		parameter = parameter.replaceAll("\\\\", "");
		JSONParser parser = new JSONParser(parameter);
		Object object;
		try {
			object = parser.parse();
			FachadaFramework.logger.debug("Chamada de Consulta com o JSON: " + parameter);
			map = JSONUtils.jsonToMap(parameter);
			if (map == null) {
				throw new NenhumJSONenviadoException();
			}

			objectDTOEnviado = new ObjectDTO(map);

			listObjectDTORecebido = this.iFrameworkController.list(objectDTOEnviado);

			resultado = JSONUtils.toJson(objectDTOEnviado.getEntity(), listObjectDTORecebido);

			FachadaFramework.logger.debug("JSON retornado do banco de dados: " + resultado);

			if (resultado == null) {
				throw new NenhumRegistroEncontradoException();
			}
		} catch (ParseException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (JsonParseException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (JsonMappingException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (IOException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (VersaoInvalidaException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (NenhumJSONenviadoException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (NenhumRegistroEncontradoException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (NenhumaConsultaFornecidaException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (ChaveNaoExistenteException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		}

		return resultado;
	}

//	public String create(String parameter) {
//		String resultado = null;
//		HashMap<String, Object> map;
//		ObjectDTO objectDTOEnviado = null;
//		ObjectDTO objectDTORecebido = null;
//		List<String> elementos = new ArrayList<String>();
//
//		map = JSONUtils.jsonToMap(parameter);
//
//		if (map == null) {
//			throw new NenhumJSONenviadoException();
//		}
//
//		objectDTOEnviado = new ObjectDTO(map);
//
//		try {
//			int retorno = this.iFrameworkController.create(objectDTOEnviado);
//
//			if (retorno > -1) {
//				resultado = JSONUtils.toJson(objectDTOEnviado.getEntity(), objectDTOEnviado.getMap());
//			}
//		} catch (org.springframework.dao.EmptyResultDataAccessException e) {
//			throw new NenhumRegistroEncontradoException();
//		} catch (NenhumaConsultaFornecidaException e) {
//			e.printStackTrace();
//		} catch (NenhumRegistroInseridoException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		if (resultado == null) {
//			throw new NenhumRegistroEncontradoException();
//		}
//
//		return resultado;
//	}
//
//	public String update(String parameter) {
//		String resultado = null;
//		HashMap<String, Object> map;
//		ObjectDTO objectDTOEnviado = null;
//		ObjectDTO objectDTORecebido = null;
//		List<String> elementos = new ArrayList<String>();
//
//		map = JSONUtils.jsonToMap(parameter);
//
//		if (map == null) {
//			throw new NenhumJSONenviadoException();
//		}
//
//		objectDTOEnviado = new ObjectDTO(map);
//
//		try {
//			int retorno = this.iFrameworkController.update(objectDTOEnviado);
//
//			if (retorno > -1) {
//				resultado = JSONUtils.toJson(objectDTOEnviado.getEntity(), objectDTOEnviado.getMap());
//			}
//		} catch (org.springframework.dao.EmptyResultDataAccessException e) {
//			throw new NenhumRegistroEncontradoException();
//		} catch (NenhumaConsultaFornecidaException e) {
//			e.printStackTrace();
//		} catch (NenhumRegistroInseridoException e) {
//			e.printStackTrace();
//		}
//
//		if (resultado == null) {
//			throw new NenhumRegistroEncontradoException();
//		}
//
//		return resultado;
//	}
//
//	public String remove(String parameter) {
//		String resultado = null;
//		HashMap<String, Object> map;
//		ObjectDTO objectDTOEnviado = null;
//		ObjectDTO objectDTORecebido = null;
//		List<String> elementos = new ArrayList<String>();
//
//		map = JSONUtils.jsonToMap(parameter);
//
//		if (map == null) {
//			throw new NenhumJSONenviadoException();
//		}
//
//		objectDTOEnviado = new ObjectDTO(map);
//
//		try {
//			int retorno = this.iFrameworkController.delete(objectDTOEnviado);
//
//			if (retorno > -1) {
//				resultado = JSONUtils.toJson(objectDTOEnviado.getEntity(), objectDTOEnviado.getMap());
//			}
//		} catch (org.springframework.dao.EmptyResultDataAccessException e) {
//			throw new NenhumRegistroEncontradoException();
//		} catch (NenhumaConsultaFornecidaException e) {
//			e.printStackTrace();
//		} catch (NenhumRegistroInseridoException e) {
//			e.printStackTrace();
//		}
//
//		if (resultado == null) {
//			throw new NenhumRegistroEncontradoException();
//		}
//
//		return resultado;
//	}

	public String call(String parameter) {
		String resultado = null;
		HashMap<String, Object> map;
		ObjectDTO objectDTOEnviado = null;
		List<ObjectDTO> listObjectDTORecebido = null;
		List<String> elementos = new ArrayList<String>();
		parameter = parameter.replaceAll("\\\\", "");
		JSONParser parser = new JSONParser(parameter);
		Object object;

		try {
			object = parser.parse();
			
			FachadaFramework.logger.debug("Chamada de Consulta com o JSON: " + parameter);
			
			map = JSONUtils.jsonToMap(parameter);

			if (map == null) {
				throw new NenhumJSONenviadoException();
			}

			objectDTOEnviado = new ObjectDTO(map);

				listObjectDTORecebido = this.iFrameworkController.call(objectDTOEnviado);

			if (listObjectDTORecebido == null) {
				throw new NenhumRegistroEncontradoException();
			}

			resultado = JSONUtils.toJson(objectDTOEnviado.getEntity(), listObjectDTORecebido);

			FachadaFramework.logger.debug("JSON retornado do banco de dados: " + resultado);

			if (resultado == null) {
				throw new NenhumRegistroEncontradoException();
			}
		} catch (ParseException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (NenhumJSONenviadoException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (JsonParseException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (JsonMappingException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (IOException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (VersaoInvalidaException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (NenhumRegistroEncontradoException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (NenhumaConsultaFornecidaException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (ChaveNaoExistenteException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (TipoCallableIndefinidoException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		} catch (java.text.ParseException e) {
			resultado = JSONUtils.exceptionToJson(new MVQLException(e));
		}

		return resultado;
	}


}
