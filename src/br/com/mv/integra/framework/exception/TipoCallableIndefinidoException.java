package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class TipoCallableIndefinidoException extends Exception {

	public TipoCallableIndefinidoException() {
		super(FachadaFramework.getResourceBundle().getString("tipo_callable_nao_definido"));
	}

}
