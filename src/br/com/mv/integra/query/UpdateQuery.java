package br.com.mv.integra.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class UpdateQuery extends Query {
	String from;
	List<String> setClauses;
	List<Object> setValueClauses;
	List<String> whereClauses;
	List<Object> valueClauses;
	
	public UpdateQuery parse(HashMap<String, Object> map, String returnColumn) throws JsonParseException, JsonMappingException, IOException {
		UpdateQuery resultado = null;

		resultado = new UpdateQuery();

		for (String key : map.keySet()) {
			resultado.from = key;
		}

		HashMap<String,Object> child = (HashMap<String, Object>) map.get(resultado.from);

		resultado.setClauses = new ArrayList<String>();
		resultado.setValueClauses = new ArrayList<Object>();
		resultado.whereClauses = new ArrayList<String>();
		resultado.valueClauses = new ArrayList<Object>();

		for (String set : child.keySet()) {
			Object setValueClause = child.get(set);

			if(setValueClause instanceof HashMap) {
				HashMap<String, Object> where = (HashMap<String, Object>) setValueClause;
				
				for(String whereClause : where.keySet()) {
					Object whereValue = where.get(whereClause);
					
					resultado.whereClauses.add(whereClause);
					resultado.valueClauses.add(whereValue);
				}
			} else {
				resultado.setClauses.add(set);
				resultado.setValueClauses.add(setValueClause);
			}
		}

		return resultado;
	}
	
	public String generateSQL() {
		String resultado = "";

		resultado += UPDATE + SP + this.from + SP;
		
		Iterator iteratorSetClauses = this.setClauses.iterator();
		Iterator iteratorSetValueClauses = this.setValueClauses.iterator();

		if(iteratorSetClauses.hasNext()) resultado += SET + SP;
		
		while (iteratorSetClauses.hasNext()) {
			Object value = iteratorSetValueClauses.next();
			
			if (value instanceof String) {
				String date = null, time = null;
				Scanner scanner = new Scanner(value.toString());
				try {
					date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
				} catch (java.util.InputMismatchException e) {
				}

				if (date != null) {
					date = value.toString().substring(0, value.toString().length() - 2);
					resultado += iteratorSetClauses.next() + SP + EQUALS + SP + TO_DATE + LPAR + QUOTE + date + QUOTE
							+ COMMA + QUOTE + DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR + SP;
				} else {
					resultado += iteratorSetClauses.next() + SP + EQUALS + SP + QUOTE + value.toString()
							+ QUOTE + SP;
				}
			} else if (value instanceof Integer) {
				resultado += iteratorSetClauses.next() + SP + EQUALS + SP + new Integer(value.toString()) + SP;
			}
			
			if (iteratorSetClauses.hasNext())
				resultado += COMMA + SP;
		}

		resultado += SP;
		
		Iterator iteratorWhereClauses = this.whereClauses.iterator();
		Iterator iteratorValueClauses = this.valueClauses.iterator();
		
		if(iteratorWhereClauses.hasNext()) resultado += WHERE + SP;
		
		while(iteratorWhereClauses.hasNext()) {
			Object whereValueClause = iteratorValueClauses.next();
			
			if (whereValueClause instanceof String) {
				String date = null, time = null;
				Scanner scanner = new Scanner(whereValueClause.toString());
				try {
					date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
				} catch (java.util.InputMismatchException e) {
				}

				if (date != null) {
					date = whereValueClause.toString().substring(0, whereValueClause.toString().length() - 2);
					resultado += iteratorWhereClauses.next() + SP + GREATER + SP + TO_DATE + LPAR + QUOTE + date + QUOTE
							+ COMMA + QUOTE + DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR + SP;
				} else {
					resultado += iteratorWhereClauses.next() + SP + LIKE + SP + QUOTE + LIKE_TOKEN + whereValueClause.toString()
							+ LIKE_TOKEN + QUOTE + SP;
				}
			} else if (whereValueClause instanceof Integer) {
				resultado += iteratorWhereClauses.next() + SP + EQUALS + SP + new Integer(whereValueClause.toString()) + SP;
			}
			
			if (iteratorSetClauses.hasNext())
				resultado += COMMA + SP;
		}
		
		return resultado;
	}
	
	public String toValue(Object value) {
		String resultado = "";
		
		if (value instanceof String) {
			String date = null, time = null;
			Scanner scanner = new Scanner(value.toString());
			try {
				date = scanner.next(Pattern.compile("\\d{4}-\\d{2}-\\d{2}"));
			} catch (java.util.InputMismatchException e) {
			}

			if (date != null) {
				date = value.toString().substring(0, value.toString().length() - 2);
				resultado = TO_DATE + LPAR + QUOTE + date + QUOTE + COMMA + QUOTE
						+ DATE_FORMAT_YYYY_MM_DD_HH24_MI_SS + QUOTE + RPAR;
			} else {
				resultado = QUOTE + value.toString() + QUOTE;
			}

		} else if (value instanceof Integer) {
			resultado = value.toString();
		} else if (value instanceof ReservedCommandsQuery) {
			resultado = ((ReservedCommandsQuery) value).generateSQL();
		}

		return resultado;
	}

}
