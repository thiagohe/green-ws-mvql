package br.com.mv.integra.framework.exception;

import br.com.mv.integra.framework.FachadaFramework;

public class VersaoInvalidaException extends Exception {
	public VersaoInvalidaException() {
		super(FachadaFramework.getResourceBundle().getString("versao_invalida"));
	}

}
